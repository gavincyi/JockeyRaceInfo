package pkg;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class RaceInfoMain {
	private static final String LOG4J_CONFIG_FILE = "log4j.properties";
	private static Logger logger_ = Logger.getLogger(RaceInfoMain.class);

	private static Properties properties_ = new Properties();

	public static Properties getProperties() {
		return properties_;
	}

	private static Integer check_integer(String inputLine) {
		Integer temp = null;
		if (inputLine.matches(".*\\d+")) {
			temp = Integer.parseInt(inputLine);
		}
		return temp;
	}

	public static void main(String[] args) {
		try {
			PropertyConfigurator.configure(LOG4J_CONFIG_FILE);
		} catch (Exception ex) {
			ex.printStackTrace();
			return;
		}

		final int load_wait_time = 60 * 1000;

		try {
			properties_.load(new FileInputStream("config.properties"));
			logger_.info("Main start");
			DbUtil.INST.init();
			try (Connection conn = DbUtil.INST.getConn(); Statement stmt = conn.createStatement()) {

				String query = "select table_name from information_schema.tables where TABLE_SCHEMA = '"
						+ DbUtil.INST.getSchemaName() + "' and "
						+ "TABLE_NAME like 'hkjc\\_race\\_info\\_________' order by table_name DESC limit 1";

				String table = null;
				try (ResultSet rs = stmt.executeQuery(query)) {
					if (rs.next()) {
						table = rs.getString(1);
					}
				}
				logger_.info("last local race table: " + table);
				String pre_date = table.substring(table.lastIndexOf("_") + 1);
				RaceInfo race_info = RaceInfo.get_next_race_info(load_wait_time);
				String race_date = race_info.get_race_date();

				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, +1);
				String ytd = new SimpleDateFormat("yyyyMMdd").format(cal.getTime());
				if (race_date != null && Integer.parseInt(ytd) == Integer.parseInt(race_date)) {
					String venue = race_info.get_venue();
					int total_races = race_info.get_total_races();
					logger_.info("total races:" + total_races);

					get_past_incidents(race_date, venue, load_wait_time, stmt, total_races);
					get_veterinary_rec(race_date, venue, load_wait_time, stmt, total_races);
					get_form_line_report(race_date, venue, load_wait_time, stmt, total_races);
					get_except_factors(race_date, venue, load_wait_time, stmt, total_races);
					get_swimming(race_date, venue, load_wait_time, stmt, total_races);
					get_trackwork_info(race_date, pre_date, load_wait_time, stmt, conn);
					get_formguide(race_date, venue, load_wait_time, stmt, total_races);
					get_speedguide(race_date, venue, load_wait_time, stmt, total_races);
					get_bt_result(race_date, pre_date, load_wait_time, stmt, conn);

				}
			}
		} catch (Exception e) {
			logger_.error("Exception: ", e);
		} finally {
			DbUtil.INST.free();
			logger_.info("stopped");
		}

	}

	public static void get_speedguide(String race_date, String venue, int load_wait_time, Statement stmt,
			int total_races) {
		logger_.info("Start getting the speedguide record for next race " + race_date);
		boolean loading = false;
		try {
			String table_name = "speedguide_" + race_date;
			String sql = "CREATE TABLE if not exists " + table_name
					+ " (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, race INT, horse_no INT, horse varchar(30), draw INT, energy_required INT, "
					+ "last_run5_energy INT, last_run5_venue varchar(50), last_run5_dist INT, last_run5_course varchar(10), last_run4_energy INT, "
					+ "last_run4_venue varchar(50), last_run4_dist INT, last_run4_course varchar(10), last_run3_energy INT, last_run3_venue varchar(50), "
					+ "last_run3_dist INT, last_run3_course varchar(10), last_run2_energy INT, last_run2_venue varchar(50), last_run2_dist INT, "
					+ "last_run2_course varchar(10), last_run1_energy INT, last_run1_venue varchar(50), last_run1_dist INT, last_run1_course varchar(10), "
					+ "best_last_12mon_energy INT, best_last_12mon_venue varchar(50), best_last_12mon_dist INT, best_last_12mon_course varchar(10), "
					+ "best_at_dist_energy INT, best_at_dist__venue varchar(50), best_at_dist_dist INT, best_at_dist_course varchar(10), speed_energy INT, "
					+ "energy_diff INT)";
			stmt.executeUpdate(sql);

			for (int race = 1; race <= total_races; race++) {
				do {
					sql = null;
					loading = false;

					String url_addr = "http://www.hkjc.com/english/speedguide/" + race + ".html";
					logger_.info(url_addr);

					List<String> line_arr = URLConn.load_url(url_addr, load_wait_time);
					int tr_cnt = 0;
					int line_cnt = 0;
					Integer horse_no = null;
					String horse = "";
					Integer draw = null;
					Integer energy = null;
					Integer[] last_energy = new Integer[7];
					String[] last_venue = new String[7];
					Integer[] last_dist = new Integer[7];
					String[] last_course = new String[7];
					Integer speed_energy = null;
					Integer diff = null;
					int idx = 0;

					for (String inputLine : line_arr) {
						if (inputLine.contains("Loading.gif")) {
							logger_.info("the page is still loading");
							loading = true;
							break;
						}

						if (tr_cnt > 0 && inputLine.contains("</table>")) {
							break;
						}
						if (tr_cnt > 1 && inputLine.contains("</tr>")) {
							if (sql == null) {
								sql = "insert into " + table_name + " values ";
							}

							sql += "(NULL, " + race + ", " + horse_no + ", \"" + horse + "\", " + draw + ", " + energy
									+ ", ";
							for (int i = 0; i < 7; i++) {
								sql += last_energy[i] + ", \"" + last_venue[i] + "\", " + last_dist[i] + ", \""
										+ last_course[i] + "\",";
							}
							sql += speed_energy + ", " + diff + ")";
							if (sql.length() > 2000) {
								sql += ";";
								stmt.execute(sql);
								sql = null;
							} else {
								sql += ",";
							}

							line_cnt = 0;
						} else if (inputLine.contains("font_size_10")) {
							tr_cnt = 1;
						} else if (tr_cnt > 0 && inputLine.contains("<tr")) {
							++tr_cnt;
						} else if (tr_cnt > 1) {
							++line_cnt;
							if (inputLine.contains("</td"))
								inputLine = inputLine.substring(0, inputLine.indexOf("</td"));
							if (inputLine.contains("<td"))
								inputLine = inputLine.substring(inputLine.indexOf(">") + 1);
							inputLine = inputLine.trim();
							switch (line_cnt) {
							case 1:
								horse_no = check_integer(inputLine);
								break;
							case 2:
								horse = inputLine;
								break;
							case 3:
								draw = check_integer(inputLine);
								break;
							case 4:
								energy = check_integer(inputLine);
								break;
							case 5:
							case 6:
							case 7:
							case 8:
							case 9:
								String[] arr = inputLine.split("</br>");
								last_energy[line_cnt - 5] = check_integer(arr[0]);
								if (arr.length == 3) {
									last_venue[line_cnt - 5] = arr[1].substring(0, arr[1].indexOf(" "));
									last_dist[line_cnt - 5] = check_integer(arr[1].substring(arr[1].indexOf(" ") + 1));
								} else {
									last_venue[line_cnt - 5] = "";
									last_dist[line_cnt - 5] = null;
								}
								last_course[line_cnt - 5] = (arr.length == 3) ? arr[2] : "";
								break;
							case 11:
							case 16:
								inputLine = inputLine.substring(0, inputLine.indexOf("<"));
								idx = (line_cnt == 11 ? 5 : 6);
								last_energy[idx] = check_integer(inputLine);
								break;
							case 12:
							case 17:
								inputLine = inputLine.substring(0, inputLine.indexOf("<"));
								idx = (line_cnt == 12 ? 5 : 6);
								if (inputLine.contains(" ")) {
									last_venue[idx] = inputLine.substring(0, inputLine.indexOf(" "));
									last_dist[idx] = check_integer(inputLine.substring(inputLine.indexOf(" ") + 1));
								} else {
									last_venue[idx] = "";
									last_dist[idx] = null;
								}
								break;
							case 13:
							case 18:
								idx = (line_cnt == 13 ? 5 : 6);
								last_course[idx] = inputLine;
								break;
							case 21:
								speed_energy = check_integer(inputLine);
								break;
							case 22:
								diff = check_integer(inputLine);
								break;
							default:
								break;
							}
						}

					}
					if (sql != null) {
						sql = sql.substring(0, sql.length() - 1) + ";";
						stmt.execute(sql);
					}
				} while (loading);
			}
			logger_.info("Finish getting the speedguide record for next race " + race_date);
		} catch (Exception e) {
			logger_.error("Exception:", e);
		}
	}

	public static void get_formguide(String race_date, String venue, int load_wait_time, Statement stmt,
			int total_races) {
		logger_.info("Start getting the formguide record for next race " + race_date);
		boolean loading = false;
		try {
			String table_name = "formguide_" + race_date;
			String sql = "CREATE TABLE if not exists " + table_name
					+ " (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, race INT, horse_no INT, horse varchar(30), draw INT,  body_wt INT, wt INT, "
					+ "jockey varchar(30), trainer varchar(30), age INT, last_date varchar(10), days INT, last_venue varchar(5), last_course varchar(10), last_dist INT,"
					+ "last_going varchar(20), last_draw INT, last_body_wt INT, last_wt INT, last_joc varchar(30), last_pla INT, total_horses INT, energy INT,"
					+ "sect_time Text, detail Text, odd DOUBLE PRECISION(10,2))";
			stmt.executeUpdate(sql);
			String decimalPattern = "([0-9]*)\\.?([0-9]*)";

			for (int race = 1; race <= total_races; race++) {
				do {
					sql = null;
					loading = false;
					String url_addr = "http://www.hkjc.com/english/formguide/" + race + ".html";
					logger_.info(url_addr);

					List<String> line_arr = URLConn.load_url(url_addr, load_wait_time);
					int tr_type = 0;
					int td_cnt = 0;
					Integer horse_no = null;
					String horse = "";
					Integer draw = null;
					Integer body_wgt = null;
					Integer wgt = null;
					String jockey = "";
					String trainer = "";
					Integer age = null;
					String last_date = "";
					Integer days = null;
					String last_venue = "";
					String last_course = "";
					Integer last_dist = null;
					String last_going = "";
					Integer last_draw = null;
					Integer last_body_wgt = null;
					Integer last_wgt = null;
					String last_jockey = "";
					Integer last_pla = null;
					Integer total_horse = null;
					Integer energy = null;
					String sect_time = "";
					String detail = "";
					Double odd = null;

					for (String inputLine : line_arr) {
						if (inputLine.contains("Loading.gif")) {
							logger_.info("the page is still loading");
							loading = true;
							break;
						}

						if (inputLine.contains("runner-header")) {
							tr_type = 1;
						} else if (inputLine.contains("runner-form-entry")) {
							tr_type = 2;
						} else if (inputLine.contains("starter-comment")) {
							last_date = "";
							days = null;
							last_venue = "";
							last_course = "";
							last_dist = null;
							last_going = "";
							last_draw = null;
							last_body_wgt = null;
							last_wgt = null;
							last_jockey = "";
							last_pla = null;
							total_horse = null;
							energy = null;
							sect_time = "";
							detail = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("</"));
							odd = null;
							tr_type = 3;
						} else if (tr_type > 0) {
							if (inputLine.contains("</tr>")) {
								if (tr_type == 2 || tr_type == 3) {
									if (sql == null) {
										sql = "insert into " + table_name + " values ";
									}
									sql += "(NULL, " + race + ", " + horse_no + ",\"" + horse + "\"," + draw + ","
											+ body_wgt + "," + wgt + ",\"" + jockey + "\",\"" + trainer + "\"," + age
											+ ", \"" + last_date + "\", " + days + ",\"" + last_venue + "\",\""
											+ last_course + "\"," + last_dist + ",\"" + last_going + "\"," + last_draw
											+ "," + last_body_wgt + "," + last_wgt + ",\"" + last_jockey + "\","
											+ last_pla + ", " + total_horse + ", " + energy + ",\"" + sect_time
											+ "\",\"" + detail + "\"," + odd + ")";

									if (sql.length() > 2000) {
										sql += ";";
										stmt.execute(sql);
										sql = null;
									} else {
										sql += ",";
									}

								}
								tr_type = 0;
								td_cnt = 0;
							} else {
								++td_cnt;
								inputLine = StringEscapeUtils.unescapeHtml4(inputLine);
								if (inputLine.contains("</"))
									inputLine = inputLine.substring(0, inputLine.indexOf("</"));
								if (inputLine.contains("<"))
									inputLine = inputLine.substring(inputLine.indexOf(">") + 1);
								inputLine = inputLine.trim();
								if (tr_type == 1) {
									switch (td_cnt) {
									case 1:
										horse_no = check_integer(inputLine.substring(0, inputLine.indexOf(" ")));
										horse = inputLine.substring(inputLine.indexOf(" ") + 1);
										break;
									case 2:
										inputLine = inputLine.substring(inputLine.indexOf("(") + 1,
												inputLine.indexOf(")"));
										draw = check_integer(inputLine);
										break;
									case 3:
										body_wgt = check_integer(inputLine);
										break;
									case 4:
										wgt = check_integer(inputLine);
										break;
									case 5:
										jockey = inputLine;
										break;
									case 6:
										trainer = inputLine;
										break;
									case 7:
										age = check_integer(inputLine.substring(inputLine.indexOf(":") + 2));
										break;
									default:
										break;
									}
								} else if (tr_type == 2) {
									switch (td_cnt) {
									case 1:
										last_date = new SimpleDateFormat("yyyyMMdd")
												.format(new SimpleDateFormat("dd/MM/yyyy").parse(inputLine));
										break;
									case 2:
										days = check_integer(inputLine);
										break;
									case 3:
										last_venue = inputLine.substring(0, inputLine.indexOf(" "));
										last_course = inputLine.substring(inputLine.indexOf(" ") + 2,
												inputLine.lastIndexOf("\""));
										last_dist = check_integer(inputLine.substring(inputLine.lastIndexOf("\"") + 2,
												inputLine.lastIndexOf(" ")));
										last_going = inputLine.substring(inputLine.lastIndexOf(" ") + 1);
										break;
									case 4:
										last_draw = check_integer(inputLine);
										break;
									case 5:
										last_body_wgt = check_integer(inputLine);
										break;
									case 6:
										last_wgt = check_integer(inputLine);
										break;
									case 7:
										last_jockey = inputLine;
										break;
									case 8:
										last_pla = check_integer(inputLine.substring(0, inputLine.indexOf(" ")));
										total_horse = check_integer(
												inputLine.substring(inputLine.lastIndexOf(" ") + 1));
										break;
									case 9:
										energy = check_integer(inputLine);
										break;
									case 10:
										sect_time = inputLine.substring(0, inputLine.indexOf("<br>"));
										detail = inputLine.substring(inputLine.indexOf("<br>") + 4);
										break;
									case 11:
										if (Pattern.matches(decimalPattern, inputLine))
											odd = Double.parseDouble(inputLine);
										else
											odd = null;
										break;
									default:
										break;

									}
								}
							}
						}
					}
					if (sql != null) {
						sql = sql.substring(0, sql.length() - 1) + ";";
						stmt.execute(sql);
					}

				} while (loading);
			}

			logger_.info("Finish getting the formguide record for next race " + race_date);
		} catch (Exception e) {
			logger_.error("Exception:", e);
		}
	}

	public static void get_swimming(String race_date, String venue, int load_wait_time, Statement stmt,
			int total_races) {
		logger_.info("Start getting the swimming record for next race " + race_date);
		boolean loading = false;
		try {
			String table_name = "swim_" + race_date;
			String sql = "CREATE TABLE if not exists " + table_name
					+ " (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, race INT, horse_no INT, horse varchar(30), swim_date varchar(10), "
					+ "standby varchar(5))";
			stmt.executeUpdate(sql);

			for (int race = 1; race <= total_races; race++) {
				do {
					sql = null;
					loading = false;
					String url_addr = "http://racing.hkjc.com/racing/Info/Meeting/Trackwork/English/Local/" + race_date
							+ "/" + venue + "/" + race;
					logger_.info(url_addr);
					List<String> line_arr = URLConn.load_url(url_addr, load_wait_time);
					int tr_cnt = 0;
					int td_cnt = 0;
					int line_cnt = 0;
					String standby = "N";
					Integer horse_no = null;
					String horse = "";
					String date_str = "";

					for (String inputLine : line_arr) {
						if (inputLine.contains("Loading.gif")) {
							logger_.info("the page is still loading");
							loading = true;
							break;
						}

						if (inputLine.contains("STAND-BY")) {
							standby = "Y";
						} else if ((tr_cnt == 0 && inputLine.contains("boldFont13"))
								|| (tr_cnt > 0 && inputLine.contains("<tr"))) {
							++tr_cnt;
						} else if (inputLine.contains("</table>")) {
							tr_cnt = 0;
						} else if (inputLine.contains("</tr>")) {
							if (tr_cnt > 1 && !date_str.equals("")) {
								String[] arr = date_str.split("</b><br />");
								if (sql == null) {
									sql = "insert into " + table_name + " values ";
								}
								for (int i = 0; i < arr.length; i++) {
									arr[i] = arr[i].substring(arr[i].indexOf(">") + 1);
									arr[i] = new SimpleDateFormat("MMdd")
											.format(new SimpleDateFormat("dd/MM").parse(arr[i]));
									sql += "(NULL, " + race + "," + horse_no + ",\"" + horse + "\",\"" + arr[i]
											+ "\",\"" + standby + "\"),";
								}
								if (sql.length() > 2000) {
									sql = sql.substring(0, sql.length() - 1) + ";";
									stmt.execute(sql);
									sql = null;
								}

							}
							date_str = "";
							td_cnt = 0;
						} else if (inputLine.contains("</td>")) {
							line_cnt = 0;
						} else if (tr_cnt > 1 && inputLine.contains("<td")) {
							++td_cnt;
						} else if (td_cnt == 1) {
							++line_cnt;
							if (line_cnt == 1) {
								horse_no = check_integer(inputLine.trim());
							}
						} else if (td_cnt == 2) {
							++line_cnt;
							if (line_cnt == 1) {
								inputLine = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("</"));
								horse = inputLine;
							}
						} else if (td_cnt == 6) {
							inputLine = inputLine.trim();
							date_str += inputLine;
						}

					}

					if (sql != null) {
						sql = sql.substring(0, sql.length() - 1) + ";";
						stmt.execute(sql);
					}

				} while (loading);
			}
			logger_.info("Finish getting the swimming record for next race " + race_date);
		} catch (Exception e) {
			logger_.error("Exception:", e);
		}
	}

	public static void get_past_incidents(String race_date, String venue, int load_wait_time, Statement stmt,
			int total_races) {
		logger_.info("Start getting the past incident for next race " + race_date);
		boolean loading = false;
		try {
			String table_name = "past_incidents_" + race_date;
			String sql = "CREATE TABLE if not exists " + table_name
					+ " (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, venue varchar(5), race INT, horse_no INT, brand_no varchar(10), "
					+ "horse varchar(30), race_date varchar(10), race_index INT, description text, standby varchar(5))";
			stmt.executeUpdate(sql);

			for (int race = 1; race <= total_races; race++) {
				do {
					sql = null;
					loading = false;
					String url_addr = "http://racing.hkjc.com/racing/Info/Meeting/RaceReportExt/English/Local/"
							+ race_date + "/" + venue + "/" + race;
					logger_.info(url_addr);
					List<String> line_arr = URLConn.load_url(url_addr, load_wait_time);
					boolean begin = false;
					int tr_cnt = 0;
					int td_cnt = 0;
					Integer horse_no = null;
					String brand_no = "";
					String horse = "";
					String last_date = "";
					Integer race_idx = null;
					String description = "";
					String standby = "N";

					for (String inputLine : line_arr) {
						if (inputLine.contains("Loading.gif")) {
							logger_.info("the page is still loading");
							loading = true;
							break;
						}

						if (inputLine.contains("width=\"760px\" ")) {
							begin = true;
						} else if (begin) {
							if (inputLine.contains("<tr")) {
								++tr_cnt;
							} else if (inputLine.contains("</tr>")) {
								if (td_cnt > 1) {
									if (sql == null) {
										sql = "insert into " + table_name + " values ";
									}
									sql += "(NULL, \"" + venue + "\"," + race + "," + horse_no + ",\"" + brand_no
											+ "\",\"" + horse + "\",\"" + last_date + "\"," + race_idx + ",\""
											+ description + "\",\"" + standby + "\")";
									if (sql.length() > 2000) {
										sql += ";";
										stmt.execute(sql);
										sql = null;
									} else {
										sql += ",";
									}
								}
								td_cnt = 0;
							} else if (inputLine.contains("</table>")) {
								tr_cnt = 0;
								begin = false;
							} else if (tr_cnt > 1 && (inputLine.contains("<td") || td_cnt > 1)) {
								++td_cnt;
								inputLine = inputLine.replace("&nbsp;", "");
								if (inputLine.contains("</"))
									inputLine = inputLine.substring(0, inputLine.indexOf("</"));

								if (inputLine.contains("<"))
									inputLine = inputLine.substring(inputLine.indexOf(">") + 1);

								if (inputLine.contains("<"))
									inputLine = inputLine.substring(inputLine.indexOf(">") + 1);
								switch (td_cnt) {
								case 1:
									if (inputLine.contains("Standby"))
										standby = "Y";
									else
										horse_no = check_integer(inputLine);
									break;
								case 2:
									brand_no = inputLine;
									break;
								case 3:
									horse = inputLine;
									break;
								case 4:
									if (inputLine.equals(""))
										last_date = inputLine;
									else
										last_date = new SimpleDateFormat("yyyyMMdd")
												.format(new SimpleDateFormat("dd/MM/yyyy").parse(inputLine));
									break;
								case 5:
									race_idx = check_integer(inputLine);
									break;
								case 6:
									description = inputLine;
									break;
								default:
									description += " " + inputLine;
									break;
								}
							}
						}
					}
					if (sql != null) {
						sql = sql.substring(0, sql.length() - 1) + ";";
						stmt.execute(sql);
					}

				} while (loading);
			}
			logger_.info("Finish getting the past incident for next race " + race_date);
		} catch (Exception e) {
			logger_.error("Exception:", e);
		}
	}

	public static void get_form_line_report(String race_date, String venue, int load_wait_time, Statement stmt,
			int total_races) {

		logger_.info("Start getting the form line report for next race " + race_date);
		boolean loading = false;
		try {

			String table_name = "form_line_report_" + race_date;
			String sql = "CREATE TABLE if not exists " + table_name
					+ " (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, venue varchar(5), race INT, last_race_date varchar(10), last_venue varchar(5), "
					+ "last_dist INT, last_class varchar(30), last_course varchar(10), last_going varchar(30), horse varchar(30), wt INT, last_wt INT, "
					+ "rtg INT, last_rtg INT, draw INT, last_draw INT, jockey varchar(30), last_jockey varchar(30), placing INT, total_horses INT, "
					+ "time varchar(20), margin varchar(10), win_odd DOUBLE PRECISION(10,2), wt_diff_changed varchar(4))";
			stmt.executeUpdate(sql);

			for (int race = 1; race <= total_races; race++) {
				do {
					sql = null;
					loading = false;
					String url_addr = "http://racing.hkjc.com/racing/Info/Meeting/FormLine/English/Local/" + race_date
							+ "/" + venue + "/" + race;
					logger_.info(url_addr);

					List<String> line_arr = URLConn.load_url(url_addr, load_wait_time);
					int td_cnt = 0;
					int tr_type = 0; // 1 for header, 2 for content
					String last_date = "";
					String last_venue = "";
					Integer dist = null;
					String last_class = "";
					String last_course = "";
					String last_going = "";
					String horse = "";
					Integer wgt = null;
					Integer last_wgt = null;
					Integer rtg = null;
					Integer last_rtg = null;
					Integer draw = null;
					Integer last_draw = null;
					String joc = "";
					String last_joc = "";
					Integer pla = null;
					Integer total_horses = null;
					String time = "";
					String margin = "";
					double odd = 0;
					Integer wgt_changed = null;
					String temp;

					for (String inputLine : line_arr) {
						if (inputLine.contains("Loading.gif")) {
							logger_.info("the page is still loading");
							loading = true;
							break;
						}

						if (inputLine.contains("trBgBlue1")) {
							tr_type = 1;
						} else if (inputLine.contains("number13")) {
							tr_type = 2;
						} else if (tr_type == 1 && inputLine.contains("td")) {
							++td_cnt;
							if (inputLine.contains("</")) {
								inputLine = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("</"));
							}
							switch (td_cnt) {
							case 2:
								last_date = new SimpleDateFormat("yyyyMMdd")
										.format(new SimpleDateFormat("dd/MM/yyyy").parse(inputLine));
								break;
							case 3:
								last_venue = inputLine.substring(0, 1) + inputLine.charAt(inputLine.indexOf(" ") + 1);
								break;
							case 4:
								temp = inputLine.substring(0, inputLine.indexOf("m"));
								dist = check_integer(temp);
								break;
							case 5:
								last_class = inputLine;
								break;
							case 6:
								if (inputLine.contains(";"))
									inputLine = inputLine.substring(inputLine.indexOf(";") + 1,
											inputLine.lastIndexOf("&"));
								last_course = inputLine;
								break;
							case 7:
								last_going = inputLine;
								break;
							default:
								break;
							}
						} else if (tr_type == 2 && inputLine.contains("td")) {
							++td_cnt;
							inputLine = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("</"));
							switch (td_cnt) {
							case 1:
								horse = inputLine;
								break;
							case 2:
								temp = inputLine.substring(0, inputLine.indexOf("("));
								wgt = check_integer(temp);
								temp = inputLine.substring(inputLine.indexOf("(") + 1, inputLine.indexOf(")"));
								last_wgt = check_integer(temp);
								break;
							case 3:
								temp = inputLine.substring(0, inputLine.indexOf("("));
								rtg = check_integer(temp);
								temp = inputLine.substring(inputLine.indexOf("(") + 1, inputLine.indexOf(")"));
								last_rtg = check_integer(temp);
								break;
							case 4:
								temp = inputLine.substring(0, inputLine.indexOf("("));
								draw = check_integer(temp);
								temp = inputLine.substring(inputLine.indexOf("(") + 1, inputLine.indexOf(")"));
								last_draw = check_integer(temp);
								break;
							case 5:
								joc = inputLine;
								break;
							case 6:
								last_joc = inputLine;
								break;
							case 7:
								temp = inputLine.substring(0, inputLine.indexOf("/"));
								pla = check_integer(temp);
								temp = inputLine.substring(inputLine.indexOf("/") + 1);
								total_horses = check_integer(temp);
								break;
							case 8:
								time = inputLine;
								break;
							case 9:
								margin = inputLine;
								break;
							case 10:
								odd = Double.parseDouble(inputLine);
								break;
							default:
								wgt_changed = check_integer(inputLine);
								break;
							}

						} else if (inputLine.contains("</tr>")) {
							if (tr_type == 2) {
								if (sql == null) {
									sql = "insert into " + table_name + " values ";
								}

								sql += "(NULL, \"" + venue + "\"," + race + ",\"" + last_date + "\",\"" + last_venue
										+ "\"," + dist + ",\"" + last_class + "\",\"" + last_course + "\", \""
										+ last_going + "\", \"" + horse + "\"," + wgt + "," + last_wgt + ", " + rtg
										+ ", " + last_rtg + ", " + draw + ", " + last_draw + ", \"" + joc + "\",\""
										+ last_joc + "\"," + pla + ", " + total_horses + ", \"" + time + "\",\""
										+ margin + "\"," + odd + ", " + wgt_changed + ")";

								if (sql.length() > 2000) {
									sql += ";";
									stmt.executeUpdate(sql);
									sql = null;
								} else {
									sql += ",";
								}
							}

							tr_type = 0;
							td_cnt = 0;
						}
					}

				} while (loading == true);

				if (sql != null) {
					sql = sql.substring(0, sql.length() - 1) + ";";
					stmt.executeUpdate(sql);
				}
			}
			logger_.info("Finish getting the form line report for next race " + race_date);
		} catch (Exception e) {
			logger_.error("Exception: ", e);
		}
	}

	public static void get_except_factors(String race_date, String venue, int load_wait_time, Statement stmt,
			int total_races) {

		logger_.info("Start getting the exceptional factors for next race " + race_date);
		boolean loading = false;
		try {

			String table_name = "exceptional_factors_" + race_date;
			String sql = "CREATE TABLE if not exists " + table_name
					+ " (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, venue varchar(5), race INT, horse_no INT, horse varchar(30), "
					+ "out_of_the_handicap INT, WFA INT, gear varchar(30), tongue_tie varchar(30), bit_change_from varchar(30), bit_change_to varchar(30), rtg INT, "
					+ "dist INT, days_to_last_run INT, new_trainer varchar(10))";
			stmt.executeUpdate(sql);

			for (int race = 1; race <= total_races; race++) {
				do {
					sql = null;
					loading = false;
					String url_addr = "http://racing.hkjc.com/racing/Info/Meeting/ExceptionalFactors/English/Local/"
							+ race_date + "/" + venue + "/" + race;
					logger_.info(url_addr);

					List<String> line_arr = URLConn.load_url(url_addr, load_wait_time);
					boolean line_begin = false;
					int line_cnt = 0;
					int tr_type = 0; // 1 for header, 2 for same_stable, 3 for
										// except factors
					String trainer = "";
					int horse_no = 0;
					String horse = "";
					Integer out_of_the_han = null;
					Integer WFA = null;
					String gear = null;
					String tongue = null;
					String bit_change_from = null;
					String bit_change_to = null;
					Integer rtg = null;
					Integer dist = null;
					Integer days_to = null;
					String new_trainer = null;
					String same_stable_tb = "same_stable_" + race_date;
					String temp = null;

					for (String inputLine : line_arr) {
						if (inputLine.contains("Loading.gif")) {
							logger_.info("the page is still loading");
							loading = true;
							break;
						}

						if (inputLine.contains("color_white")) {
							tr_type = 1;
						} else if (tr_type == 1 && inputLine.contains("Trainer")) {
							tr_type = 2;
							sql = "CREATE TABLE if not exists " + same_stable_tb
									+ " (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, venue varchar(5), race INT, trainer varchar(30), horse_no INT, horse varchar(30))";
							stmt.execute(sql);
							sql = null;
						} else if (tr_type == 1 && inputLine.contains("Horse No")) {
							tr_type = 3;
						} else if (tr_type > 0 && inputLine.contains("</tr>") && line_begin == true) {
							if (tr_type == 3) {
								if (sql == null) {
									sql = "insert into " + table_name + " values ";
								}
								sql += "(NULL, \"" + venue + "\"," + race + "," + horse_no + ",\"" + horse + "\","
										+ out_of_the_han + "," + WFA + ",\"" + gear + "\", \"" + tongue + "\", \""
										+ bit_change_from + "\",\"" + bit_change_to + "\"," + rtg + ", " + dist + ", "
										+ days_to + ", \"" + new_trainer + "\")";

								if (sql.length() > 2000) {
									sql += ";";
									stmt.executeUpdate(sql);
									sql = null;
								} else {
									sql += ",";
								}
							}
							line_cnt = 0;
							line_begin = false;
						} else if (inputLine.contains("trBgWhite")) {
							line_begin = true;
						} else if (line_begin) {
							++line_cnt;
							inputLine = inputLine.trim();
							if (tr_type == 2) {
								switch (line_cnt) {
								case 2:
									trainer = inputLine;
									break;
								case 4:
									String[] arr = inputLine.split("</td>");
									sql = "insert into " + same_stable_tb + " values ";
									for (int i = 0; i < arr.length; i++) {
										if (arr[i].contains("&nbsp;"))
											continue;
										temp = arr[i].substring(inputLine.indexOf(">") + 1, arr[i].indexOf(" "));
										horse_no = check_integer(temp);
										horse = arr[i].substring(arr[i].indexOf(" ") + 1);
										sql += "(NULL, \"" + venue + "\"," + race + ",\"" + trainer + "\", " + horse_no
												+ ",\"" + horse + "\"),";
									}

									sql = sql.substring(0, sql.length() - 1) + ";";
									stmt.execute(sql);
									sql = null;
									break;
								default:
									break;
								}
							} else if (tr_type == 3) {
								switch (line_cnt) {
								case 2:
									horse_no = check_integer(inputLine);
									break;
								case 5:
									horse = inputLine;
									break;
								case 8:
									out_of_the_han = check_integer(inputLine);
									break;
								case 11:
									WFA = check_integer(inputLine);
									break;
								case 14:
									gear = inputLine;
									break;
								case 17:
									tongue = inputLine;
									break;
								case 20:
									bit_change_from = inputLine;
									break;
								case 23:
									bit_change_to = inputLine;
									break;
								case 26:
									rtg = check_integer(inputLine);
									break;
								case 29:
									dist = check_integer(inputLine);
									break;
								case 32:
									days_to = check_integer(inputLine);
									break;
								case 35:
									new_trainer = inputLine;
									break;
								default:
									break;
								}
							}

						} else if (inputLine.contains("</table>"))
							tr_type = 0;
					}

				} while (loading == true);

				if (sql != null) {
					sql = sql.substring(0, sql.length() - 1) + ";";
					stmt.executeUpdate(sql);
				}
			}
			logger_.info("Finish getting the exceptional factors for next race " + race_date);
		} catch (Exception e) {
			logger_.error("Exception: ", e);
		}
	}

	public static void get_bt_result(String race_date, String pre_date, int load_wait_time, Statement stmt,
			Connection con) {
		logger_.info("Start getting the barrier trial result for next race " + race_date);
		boolean loading = false;

		try {

			String table_name = "barrier_trial_" + race_date;
			String pre_table = "barrier_trial_" + pre_date;
			String sql = null;
			Date last_date = new Date();

			DatabaseMetaData dbm = con.getMetaData();
			try (ResultSet tables = dbm.getTables(null, null, pre_table, null)) {
				if (tables.next()) {
					sql = "CREATE TABLE if not exists " + table_name + " like " + pre_table;
					stmt.executeUpdate(sql);
					sql = "insert into " + table_name + " select * from " + pre_table;
					stmt.executeUpdate(sql);
					sql = "select date from " + pre_table + " order by date desc limit 1";
					logger_.info(sql);
					try (ResultSet rs = stmt.executeQuery(sql)) {
						if (rs.next()) {
							last_date = new SimpleDateFormat("yyyyMMdd").parse(rs.getString(1));
						}
					}
				} else {
					sql = "CREATE TABLE if not exists " + table_name
							+ " (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, date varchar(10), batch INT, venue varchar(10), track varchar(10), dist INT, going varchar(255), time varchar(20),"
							+ "sect_time1 varchar(10), sect_time2 varchar(10), sect_time3 varchar(10), sec_time4 varchar(10), horse varchar(30), jockey varchar(30), trainer varchar(30), "
							+ "draw INT, gear varchar(20), LBW varchar(20), running_postion varchar(20), finish_time varchar(20), result varchar(40), comment varchar(200))";
					stmt.execute(sql);
					last_date = new SimpleDateFormat("yyyyMMdd").parse("00000000");
				}
			}

			ArrayList<String> date_list = new ArrayList<String>();
			String basic = "http://www.hkjc.com/english/racing/btresult.asp";

			do {
				loading = false;

				List<String> line_arr = URLConn.load_url(basic, load_wait_time);
				for (String inputLine : line_arr) {
					if (inputLine.contains("Loading.gif")) {
						logger_.info("the page is still loading");
						loading = true;
						break;
					}

					if (inputLine.contains("option")) {
						inputLine = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("</"));
						if (inputLine.contains(">"))
							inputLine = inputLine.substring(inputLine.indexOf(">") + 1);

						Date temp_date = new SimpleDateFormat("dd/MM/yyyy").parse(inputLine);
						if (temp_date.after(last_date)) {
							date_list.add(inputLine);
						} else {
							break;
						}
					}
				}
			} while (loading);

			Date temp_day = new Date();
			for (int day = 0; day < Math.max(date_list.size(), 1); day++) {
				String url_addr = basic + "?date=" + date_list.get(day);
				logger_.info(url_addr);
				String date = new SimpleDateFormat("yyyyMMdd")
						.format(new SimpleDateFormat("dd/MM/yyyy").parse(date_list.get(day)));
				if (date.equals(new SimpleDateFormat("yyyyMMdd").format(temp_day))) {
					Date curr_time = new Date();
					Date dt = new SimpleDateFormat("yyyyMMdd HH:mm").parse(date + " 18:01");
					if (dt.after(curr_time)) {
						Thread.sleep(dt.getTime() - curr_time.getTime());
					}
				}
				do {

					loading = false;
					sql = null;
					List<String> line_arr = URLConn.load_url(url_addr, load_wait_time);
					String temp;
					Integer batch = null;
					String venue = "";
					String track = "";
					Integer dist = null;
					int td_cnt = 0;
					String going = "";
					String time = "";
					String[] sect_time = new String[4];
					int header_begin = 0;
					String horse = "";
					String trainer = "";
					String jockey = "";
					Integer draw = null;
					String gear = "";
					String lbw = "";
					String run_pos = "";
					String finish_time = "";
					String result = "";
					String comment = "";

					for (String inputLine : line_arr) {
						if (inputLine.contains("Loading.gif")) {
							logger_.info("the page is still loading");
							loading = true;
							break;
						}

						if (inputLine.contains("=subheader")) {
							temp = inputLine.substring(inputLine.indexOf(";") + 1, inputLine.lastIndexOf("&"));
							batch = check_integer(temp);
							inputLine = inputLine.substring(inputLine.indexOf("-") + 2);
							venue = inputLine.substring(0, 1) + inputLine.charAt(inputLine.indexOf(" ") + 1);
							temp = inputLine.substring(0, inputLine.indexOf("-") - 1);
							if (temp.split(" ").length == 3) {
								track = "Turf";
							} else {
								track = "AWT";
							}
							temp = inputLine.substring(inputLine.indexOf("-") + 2);
							if (temp.contains("m")) {
								dist = check_integer(temp.substring(0, temp.indexOf("m")));
							} else
								dist = null;
						} else if (inputLine.contains("cellSpacing=0") && inputLine.contains("right")) {
							header_begin = 1;
						} else if (header_begin == 1 && inputLine.contains("FONT")) {
							++td_cnt;

							switch (td_cnt) {
							case 1:
								going = inputLine.substring(inputLine.indexOf(":") + 1, inputLine.lastIndexOf("&"));
								going = going.trim();
								break;
							case 2:
								time = inputLine.substring(inputLine.indexOf(":") + 2, inputLine.indexOf("</F"));
								break;
							case 3:
								inputLine = inputLine.substring(inputLine.indexOf(":") + 2, inputLine.indexOf("</F"));
								String[] arr = inputLine.split("&nbsp;&nbsp;&nbsp;");
								for (int i = 0; i < arr.length; i++) {
									sect_time[i] = arr[i];
								}
								for (int i = arr.length; i < 4; i++) {
									sect_time[i] = "";
								}
								break;
							default:
								break;
							}
						} else if (header_begin > 0 && inputLine.contains("</TABLE>")) {
							if (header_begin == 2) {
								if (sql == null) {
									sql = "insert into " + table_name + " values ";
								}

								sql += "(NULL, \"" + date + "\", " + batch + ", \"" + venue + "\", \"" + track + "\", "
										+ dist + ", \"" + going + "\", \"" + time + "\",\"";
								for (int i = 0; i < 4; i++) {
									sql += sect_time[i] + "\",\"";
								}
								sql += horse + "\",\"" + jockey + "\",\"" + trainer + "\"," + draw + ",\"" + gear
										+ "\",\"" + lbw + "\",\"" + run_pos + "\",\"" + finish_time + "\",\"" + result
										+ "\",\"" + comment + "\")";
								if (sql.length() > 2000) {
									sql += ";";
									stmt.execute(sql);
									sql = null;
								} else {
									sql += ",";
								}

							}

							td_cnt = 0;
							header_begin = 0;
						} else if (inputLine.contains("target")) {
							header_begin = 2;
							++td_cnt;
							horse = inputLine = inputLine.substring(inputLine.indexOf(">") + 1,
									inputLine.indexOf("</"));
						} else if (header_begin == 2) {
							++td_cnt;

							if (inputLine.contains("<TD"))
								inputLine = inputLine.substring(inputLine.indexOf(">") + 1);
							if (inputLine.contains("</TD>"))
								inputLine = inputLine.substring(0, inputLine.indexOf("</"));
							if (inputLine.contains(">")) {
								inputLine = inputLine.substring(inputLine.indexOf(">") + 1);
							}

							switch (td_cnt) {
							case 2:
								jockey = inputLine;
								break;
							case 3:
								trainer = inputLine;
								break;
							case 4:
								draw = check_integer(inputLine);
								break;
							case 5:
								gear = inputLine;
								break;
							case 6:
								lbw = inputLine;
								break;
							case 10:
								run_pos = inputLine.trim();
								break;
							case 12:
								finish_time = inputLine;
								break;
							case 13:
								result = inputLine;
								break;
							case 15:
								comment = inputLine;
								break;
							default:
								break;
							}
						}

					}
				} while (loading);

				if (sql != null) {
					sql = sql.substring(0, sql.length() - 1) + ";";
					stmt.executeUpdate(sql);
				}

			}
			logger_.info("Finish getting the barrier trial result for next race " + race_date);
		} catch (Exception e) {
			logger_.error("Exception: " + e);
		}

	}

	public static void get_trackwork_info(String race_date, String pre_date, int load_wait_time, Statement stmt,
			Connection con) {

		logger_.info("Start getting the trackwork for next race " + race_date);
		boolean loading = false;

		try {
			String table_name = "trackwork_" + race_date;
			String pre_table = "trackwork_" + pre_date;
			String sql = null;
			Date last_date = new Date();

			DatabaseMetaData dbm = con.getMetaData();
			try (ResultSet tables = dbm.getTables(null, null, pre_table, null)) {
				if (tables.next()) {
					sql = "CREATE TABLE if not exists " + table_name + " like " + pre_table;
					stmt.executeUpdate(sql);
					sql = "insert into " + table_name + " select * from " + pre_table;
					stmt.executeUpdate(sql);
					sql = "select date from " + pre_table + " order by date desc limit 1";
					try (ResultSet rs = stmt.executeQuery(sql)) {
						if (rs.next()) {
							last_date = new SimpleDateFormat("yyyyMMdd").parse(rs.getString(1));
						}
					}
				} else {
					sql = "CREATE TABLE if not exists " + table_name
							+ " (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, date varchar(10), horse varchar(30), trainer varchar(30), type varchar(30), venue varchar(10), track varchar(20), "
							+ "workout varchar(100), gear varchar(20))";
					stmt.execute(sql);
					last_date = new SimpleDateFormat("yyyyMMdd").parse("00000000");
				}
			}

			ArrayList<String> date_list = new ArrayList<String>();
			do {
				loading = false;

				List<String> line_arr = URLConn.load_url("http://www.hkjc.com/english/racing/Track_Search.asp",
						load_wait_time);
				for (String inputLine : line_arr) {
					if (inputLine.contains("Loading.gif")) {
						logger_.info("the page is still loading");
						loading = true;
						break;
					}

					if (inputLine.contains("option value=\"")) {
						inputLine = inputLine.substring(inputLine.indexOf("\"") + 1, inputLine.lastIndexOf("\""));

						Date temp_date = new SimpleDateFormat("dd/MM/yyyy").parse(inputLine);
						if (temp_date.after(last_date)) {
							date_list.add(inputLine);
						} else {
							break;
						}
					}
				}
			} while (loading);

			String basic = "http://www.hkjc.com/english/racing/Track_OneDayResult.asp?optOneDay=";
			for (int day = 0; day < Math.max(date_list.size(), 1); day++) {
				String url_addr = basic + date_list.get(day);
				logger_.info(url_addr);
				String date = new SimpleDateFormat("yyyyMMdd")
						.format(new SimpleDateFormat("dd/MM/yyyy").parse(date_list.get(day)));
				do {
					loading = false;
					List<String> line_arr = URLConn.load_url(url_addr, load_wait_time);
					sql = null;
					int begin = 0;
					int tr_cnt = 0;
					int line_cnt = 0;
					String horse = "";
					String trainer = "";
					String type = "";
					String venue = "";
					String track = "";
					String workout = "";
					String gear = "";

					for (String inputLine : line_arr) {
						if (inputLine.contains("Loading.gif")) {
							logger_.info("the page is still loading");
							loading = true;
							break;
						}

						if (inputLine.contains("cellspacing=1")) {
							begin = 1;
						} else if (begin == 1 && inputLine.contains("<tr")) {
							++tr_cnt;
							if (tr_cnt > 1)
								begin = 2;
						} else if (begin == 2 && inputLine.contains("</tr>")) {
							if (sql == null) {
								sql = "insert into " + table_name + " values ";
							}
							sql += "(NULL, \"" + date + "\",\"" + horse + "\",\"" + trainer + "\",\"" + type + "\",\""
									+ venue + "\",\"" + track + "\",\"" + workout + "\",\"" + gear + "\")";
							if (sql.length() > 2000) {
								sql += ";";
								stmt.execute(sql);
								sql = null;
							} else {
								sql += ",";
							}
							line_cnt = 0;
							begin = 1;
						} else if (inputLine.contains("</table>")) {
							begin = 0;
							tr_cnt = 0;
						} else if (begin == 2) {
							++line_cnt;

							if (inputLine.contains("</"))
								inputLine = inputLine.substring(0, inputLine.indexOf("</"));
							if (inputLine.contains("<"))
								inputLine = inputLine.substring(inputLine.indexOf(">") + 1);
							inputLine = inputLine.trim();
							switch (line_cnt) {
							case 2:
								horse = inputLine;
								break;
							case 4:
								trainer = inputLine;
								break;
							case 6:
								type = inputLine;
								break;
							case 8:
								venue = inputLine.substring(0, 1) + inputLine.charAt(inputLine.indexOf(" ") + 1);
								track = inputLine.substring(inputLine.indexOf(";") + 1);
								break;
							case 10:
								workout = inputLine;
								break;
							case 13:
								gear = inputLine.replace("&nbsp;", "");
								break;
							default:
								break;
							}
						}

					}
					if (sql != null) {
						sql = sql.substring(0, sql.length() - 1) + ";";
						stmt.executeUpdate(sql);
					}
				} while (loading == true);
			}

			logger_.info("Finish getting the trackwork for next race " + race_date);
		} catch (Exception e) {
			logger_.error("Exception: ", e);
		}
	}

	public static void get_veterinary_rec(String race_date, String venue, int load_wait_time, Statement stmt,
			int total_races) {
		logger_.info("Start getting the veterinary records");
		boolean loading = false;

		try {
			String table_name = "veterinary_rec_" + race_date;
			String sql = "CREATE TABLE if not exists " + table_name
					+ " (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, venue varchar(5), race INT, horse_no INT, horse varchar(30), date varchar(10), "
					+ "details varchar(200), passed_date varchar(10), stand_by varchar(1), after_last_run varchar(1))";
			stmt.executeUpdate(sql);

			for (int race = 1; race <= total_races; race++) {

				do {
					loading = false;
					// http://racing.hkjc.com/racing/Info/Meeting/VeterinaryRecord/English/Local/20160507/ST/1
					String url_addr = "http://racing.hkjc.com/racing/Info/Meeting/VeterinaryRecord/English/Local/"
							+ race_date + "/" + venue + "/" + race;
					logger_.info(url_addr);

					List<String> line_arr = URLConn.load_url(url_addr, load_wait_time);
					Integer horse_no = null;
					String horse = "";
					String date = "";
					String detail = "";
					String passed_date = "";
					int tr_cnt = 0;
					int td_cnt = 0;
					int header_tr = 1000;
					boolean tb_begin = false;
					sql = null;
					String stand_by = "N";
					String after_last_run = "";

					for (String inputLine : line_arr) {
						if (inputLine.contains("Loading.gif")) {
							logger_.info("the page is still loading");
							loading = true;
							break;
						}

						if (inputLine.contains("width=\"758px\"")) {
							tb_begin = true;
						} else if (tb_begin) {
							if (inputLine.contains("<tr")) {
								++tr_cnt;
								if (tr_cnt == 1) {
									if (inputLine.contains("Stand-by")) {
										header_tr = 2;
										stand_by = "Y";
									} else {
										header_tr = 1;
									}
								}
							} else if (tr_cnt > header_tr && inputLine.contains("td")) {
								++td_cnt;
								if (td_cnt == 3) {
									if (inputLine.contains("bold")) {
										after_last_run = "Y";
									} else {
										after_last_run = "N";
									}
								}
								inputLine = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("</"));

								switch (td_cnt) {
								case 1:
									if (!inputLine.contains("&nbsp;")) {
										horse_no = check_integer(inputLine);
									}
									break;
								case 2:
									if (!inputLine.contains("&nbsp;")) {
										horse = inputLine.substring(inputLine.indexOf(">") + 1);
									}
									break;
								case 3:
									date = new SimpleDateFormat("yyyyMMdd")
											.format(new SimpleDateFormat("dd/MM/yyyy").parse(inputLine));
									break;
								case 4:
									detail = inputLine.replace("\"", "\\\"");
									break;
								case 5:
									if (inputLine.contains("&nbsp;"))
										passed_date = "";
									else
										passed_date = new SimpleDateFormat("yyyyMMdd")
												.format(new SimpleDateFormat("dd/MM/yyyy").parse(inputLine));
									break;
								default:
									break;
								}
							} else if (inputLine.contains("</tr>")) {
								td_cnt = 0;
								if (tr_cnt > header_tr) {
									if (sql == null) {
										sql = "insert into " + table_name + " values ";
									}

									sql += "(NULL, \"" + venue + "\"," + race + "," + horse_no + ",\"" + horse + "\",\""
											+ date + "\",\"" + detail + "\",\"" + passed_date + "\",\"" + stand_by
											+ "\",\"" + after_last_run + "\")";

									if (sql.length() > 2000) {
										sql += ";";
										stmt.executeUpdate(sql);
										sql = null;
									} else {
										sql += ",";
									}
								}

							} else if (inputLine.contains("</table>")) {
								tb_begin = false;
								tr_cnt = 0;
								header_tr = 1000;
							}

						}

					}

					if (sql != null) {
						sql = sql.substring(0, sql.length() - 1) + ";";
						stmt.executeUpdate(sql);
					}

				} while (loading == true);

			}
			logger_.info("Finish getting the veterinary records");
		} catch (Exception e) {
			logger_.error("Exception: ", e);
		}
	}
}
